import { h, Component, Prop, State } from '@stencil/core';

@Component({
  tag: 'input-code',
  styleUrl: 'input-code.css'
})
export class InputCode {

  @State() showCopiedConfirm?: number;

  @Prop() inputValue: string;

  handleCopy(event) {
    const codeElParent = event.target.parentElement;

    let codeValue = event.target;
    codeValue.select();
    document.execCommand("copy");

    if (this.showCopiedConfirm) {
      window.clearTimeout(this.showCopiedConfirm);
      this.showCopiedConfirm = 0;
    }
    codeElParent.classList.add('copied-show');
    this.showCopiedConfirm = window.setTimeout(() => {
      codeElParent.classList.remove('copied-show');
      this.showCopiedConfirm = 0;
    }, 1500);
  }

  render() {
    return (
      <div class="d-flex">
        <input class="code" type="text" value={this.inputValue} readonly onClick={(event: UIEvent) => this.handleCopy(event)} aria-label="code of the icon"/>
        <span class="copied">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 64 64"><g id="a" clip-path="url(#b)"><g transform="translate(-1824)"><path d="M1852.091,36.529l-6.9-6.9-2.84,2.82,9.74,9.74,17.558-17.558-2.82-2.82Z" /></g></g></svg>
          Copied
        </span>
      </div>
    );
  }
}