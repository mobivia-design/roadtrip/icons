import { h, Component } from '@stencil/core';

@Component({
  tag: 'app-site',
})
export class AppSite {
  render() {
    return (
      <div class="site">
        <app-icons></app-icons>
      </div>
    );
  }
}