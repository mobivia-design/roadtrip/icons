import { Config } from '@stencil/core';

// https://stenciljs.com/docs/config

export const config: Config = {
  globalStyle: './node_modules/@roadtrip/components/dist/roadtrip/roadtrip.css',
  taskQueue: 'async',
  outputTargets: [
    {
      type: 'www',
      dir: 'public',
      copy: [
        { src: 'data.json', dest: './data.json' },
      ],
      // comment the following line to disable service workers in production
      serviceWorker: null,
    },
  ],
};
