<div align="center">
  <img width="124" height="124" align="center" src="illustration/roadtrip-icons.png">

  <h1 align="center">Roadtrip Icons</h1>

  <p align="center">Icon pack for Roadtrip Design System</p>
</div>
​

## :blue_book: Documentation
​
Our documentation site lives at [design.mobivia.com](https://design.mobivia.com/03cde1f3f/p/971982-developers/b/51f45a). You'll be able to find detailed documentation on getting started, all of the components, our themes, our guidelines, and more.
​

## :package: Installation

We provide 2 different way to use icons in Roadtrip Design System:
- [road-icon](https://zeroheight.com/03cde1f3f/v/0/p/971982-developers/b/70766d) - an icon Web Component from `@roadtrip/components`
- [SVG](https://zeroheight.com/03cde1f3f/v/0/p/971982-developers/b/04e4a7) - used as SVG with predefined styles from `@roadtrip/css`
​
### Web Component

Using the icon web component require the installation of `@roadtrip/components`, 
you can follow our documentation on how to [integrate our component library](https://zeroheight.com/03cde1f3f/v/0/p/574fb6-using-web-component-library/b/32da36) to you project.

### SVG

If you want to use SVG files directly, you can use predefined styles from `@roadtrip/css` by installing it in you app by following [our documentation install steps](https://zeroheight.com/03cde1f3f/v/0/p/62224e-using-css-framework).

Or, if you context isn't using roadtrip, you can just download those SVG and use them directly, see [MDN documentation](https://developer.mozilla.org/en-US/docs/Web/SVG) for more informations.

## :rocket: Usage


​
### Web Component

In the icon web component, you can just add the `name of the icon` 
you want to use in the `icon` attribute like following.
​
```html
<road-icon icon="alert-error"></road-icon>
```
​
You can find more details of the icon web component in [our documentation](https://design.mobivia.com/03cde1f3f/p/971982-developers/b/70766d) and the [list of all icons](https://mobivia-design.gitlab.io/roadtrip/icons/) to find the name of the icon you want to use.
​

### SVG

Simply find the icon you want to use in the [list of all icons](https://mobivia-design.gitlab.io/roadtrip/icons/) and copy the SVG code.

If you want to use this icon in a component from the CSS framework, copy the code markup example of [the component](https://zeroheight.com/03cde1f3f/v/0/p/97f8bd-overview) in the `CSS framework` tab and change the icon of the example by the icon you want to use.

## :page_with_curl: License
​
Copyright © 2021 [Mobivia](https://www.mobivia.com/).

Distributed under [Apache-2.0](LICENSE) license.