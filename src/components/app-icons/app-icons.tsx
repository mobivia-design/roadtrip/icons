import { h, Component, State, Listen, Host } from '@stencil/core';

@Component({
  tag: 'app-icons',
  styleUrl: 'app-icons.css',
})
export class AppIcons {
  @State() dataIcons = [];

  @State() icons = [];

  @State() selectedIcon = '';

  @State() color;

  @State() currentTheme: string = '';

  componentWillLoad() {
    return fetch('./data.json')
      .then(response => response.json())
      .then(data => {
        Object.keys(data).map(item => {
          this.dataIcons.push(item);
          this.icons = this.dataIcons;
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  @Listen('keyup', { target: 'body' })
  escListener(ev: KeyboardEvent) {
    if (ev.code === 'Escape' && this.selectedIcon.length) this.selectedIcon = '';
  }

  @Listen('click', { target: 'body' })
  handleBodyClicked() {
    if (this.selectedIcon.length) this.selectedIcon = '';
  }

  @Listen('clearToast')
  handleClearToast() {
    this.selectedIcon = '';
  }

  handleIconClick(ev: MouseEvent, name: string) {
    ev.stopPropagation();
    this.selectedIcon = name;
  }

  handleThemeChange(event: Event) {
    const selectElement = event.target as HTMLSelectElement;
    this.currentTheme = selectElement.value;
  }

  /**
   * Global Search field
   */
  private handleKeyUp = (event: UIEvent) => {
    this.icons = this.filterIcons((event.target as HTMLRoadInputElement).value, this.dataIcons);
  };

  filterIcons(query, data) {
    const search = query.trim().toLowerCase();

    let regexp = '\\b';
    for (let i in search) {
      regexp += `(${search[i]})(.*)`;
    }
    regexp += '\\b';
    const results = data.filter(iconName => new RegExp(regexp).test(iconName));

    return results;
  }

  render() {
    const themeSelection = [
      { value: 'mobivia-theme', label: 'Mobivia' },
      { value: '', label: 'Norauto' },
      { value: 'atu-theme', label: 'ATU' },
      { value: 'auto5-theme', label: 'Auto5' },
      { value: 'midas-theme', label: 'Midas' },
    ];
    return (
      <Host class={this.currentTheme}>
        <road-toolbar class="py-8">
          <div class="toolbar-wrapper d-flex align-items-center justify-content-between">
            <h1 class="h4 d-flex mb-0 ml-8">
              <svg xmlns="http://www.w3.org/2000/svg" width="32px" height="32px" class="mr-8" viewBox="0 0 512 512">
                <g id="d" clip-path="url(#e)">
                  <rect width="512" height="512" rx="64" fill="#b40063" />
                  <g transform="translate(78 78)">
                    <g transform="translate(252 -20)" clip-path="url(#a)">
                      <g transform="translate(9.968 12.188)">
                        <path d="M800,179.234V102.5l14.089,33.291,35.944,3.069-27.247,23.846L831,198.125Z" transform="translate(-749.968 -102.5)" fill="#f8ab00" />
                        <path d="M823.348,179.234l-31.011,18.891,8.226-35.419L773.316,138.86l35.934-3.069,14.1-33.291Z" transform="translate(-773.316 -102.5)" fill="#f8ab00" />
                      </g>
                    </g>
                    <path
                      d="M245.028,134.056H110.972a23.084,23.084,0,0,1,0-46.168H230.406a10.429,10.429,0,0,0,10.43-10.43V10.43A10.429,10.429,0,0,0,230.406,0H110.972a110.972,110.972,0,0,0,0,221.944H245.028a23.084,23.084,0,0,1,0,46.168H122.143a10.429,10.429,0,0,0-10.43,10.43V345.57A10.429,10.429,0,0,0,122.143,356H245.028a110.972,110.972,0,1,0,0-221.944Z"
                      transform="translate(0)"
                      fill="#fff"
                    />
                    <g transform="translate(-33.178 233.799)" clip-path="url(#b)">
                      <g transform="translate(0.178 0.201)" clip-path="url(#c)">
                        <g transform="translate(0 0)">
                          <rect width="150" height="150" fill="none" />
                          <path
                            d="M50.865,204A32.842,32.842,0,0,0,18,236.865C18,261.514,50.865,297.9,50.865,297.9S83.73,261.514,83.73,236.865A32.842,32.842,0,0,0,50.865,204Zm0,44.6A11.738,11.738,0,1,1,62.6,236.865,11.738,11.738,0,0,1,50.865,248.6Z"
                            transform="translate(24.038 -176.07)"
                            fill="#f8ab00"
                          />
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
              Roadtrip Icons
              <road-chip outline class="ml-8 mb-0 align-self-center">
                v2.29.0
              </road-chip>
            </h1>
            <div class="d-flex">
              <road-button class="mb-0 font-weight-normal" href="https://gitlab.com/mobivia-design/roadtrip/icons" rel="noopener" target="_blank">
                Gitlab
              </road-button>
              <road-select label="Brand" sizes="lg" options={themeSelection} onChange={(event: Event) => this.handleThemeChange(event)}></road-select>
            </div>
          </div>
        </road-toolbar>
        <road-grid>
          <road-row class="list-icon">
            <road-col class="col-12">
              <road-input-group class="search-field py-16 mb-8">
                <label slot="prepend" class="input-group-text" htmlFor="searchField">
                  <road-icon name="search"></road-icon>
                </label>
                <road-input label="Search icons..." class="mb-0" id="searchField" onInput={(event: UIEvent) => this.handleKeyUp(event)}></road-input>
              </road-input-group>
            </road-col>
            <road-col class="col-12">
              <road-tabs>
                <road-tab-bar selected-tab="all" class="justify-content-start mb-8">
                  <road-tab-button tab="all">
                    <road-label>All</road-label>
                  </road-tab-button>
                  <road-tab-button tab="base">
                    <road-label>Base</road-label>
                  </road-tab-button>
                  <road-tab-button tab="outline">
                    <road-label>Outline</road-label>
                  </road-tab-button>
                  <road-tab-button tab="color">
                    <road-label>Colored</road-label>
                  </road-tab-button>
                  <div class="d-inline-flex align-items-center" style={{ marginLeft: 'auto' }}>
                    <h2 class="text-content text-lowercase font-weight-normal mb-0 mr-8">
                      <strong>{this.icons.length}</strong> of {this.dataIcons.length} icons
                    </h2>
                  </div>
                </road-tab-bar>

                <road-tab tab="all">
                  <road-row>
                    {this.icons.map(iconName => (
                      <road-col class="col-6 col-sm-4 col-md-3 col-xl-2 mb-16">
                        <article class="icon-container p-16 text-center" onClick={ev => this.handleIconClick(ev, iconName)}>
                          <road-icon name={iconName} class="mb-16" size="3x" lazy></road-icon>
                          <p class="text-content mb-8">{iconName}</p>
                        </article>
                      </road-col>
                    ))}
                  </road-row>
                </road-tab>

                <road-tab tab="base">
                  <road-row>
                    {this.icons
                      .filter(iconName => !iconName.includes('-outline') && !iconName.includes('-color'))
                      .map(iconName => (
                        <road-col class="col-6 col-sm-4 col-md-3 col-xl-2 mb-16">
                          <article class="icon-container p-16 text-center" onClick={ev => this.handleIconClick(ev, iconName)}>
                            <road-icon name={iconName} class="mb-16" size="3x" lazy></road-icon>
                            <p class="text-content mb-8">{iconName}</p>
                          </article>
                        </road-col>
                      ))}
                  </road-row>
                </road-tab>
                <road-tab tab="outline">
                  <road-row>
                    {this.icons
                      .filter(iconName => iconName.includes('-outline') && !iconName.includes('-color'))
                      .map(iconName => (
                        <road-col class="col-6 col-sm-4 col-md-3 col-xl-2 mb-16">
                          <article class="icon-container p-16 text-center" onClick={ev => this.handleIconClick(ev, iconName)}>
                            <road-icon name={iconName} class="mb-16" size="3x" lazy></road-icon>
                            <p class="text-content mb-8">{iconName}</p>
                          </article>
                        </road-col>
                      ))}
                  </road-row>
                </road-tab>
                <road-tab tab="color">
                  <road-row>
                    {this.icons
                      .filter(iconName => iconName.includes('-color'))
                      .map(iconName => (
                        <road-col class="col-6 col-sm-4 col-md-3 col-xl-2 mb-16">
                          <article class="icon-container p-16 text-center" onClick={ev => this.handleIconClick(ev, iconName)}>
                            <road-icon name={iconName} class="mb-16" size="3x" lazy></road-icon>
                            <p class="text-content mb-8">{iconName}</p>
                          </article>
                        </road-col>
                      ))}
                  </road-row>
                </road-tab>
              </road-tabs>
            </road-col>
          </road-row>
          <toast-bar selectedIcon={this.selectedIcon} color={this.color}></toast-bar>
        </road-grid>
      </Host>
    );
  }
}
