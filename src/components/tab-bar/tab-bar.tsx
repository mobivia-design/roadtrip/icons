import { h, Component, Prop, State, Element } from '@stencil/core';

@Component({
  tag: 'tab-bar',
  styleUrl: 'tab-bar.css',
})
export class TabBar {
  @Element() element: HTMLElement;

  @State() showCopiedConfirm?: number;

  @State() svgCode: string;

  @Prop() selectedIcon: string;

  @Prop() color: string;

  componentDidUpdate() {
    fetch(`./build/svg/${this.selectedIcon}.svg`)
      .then(response => response.text())
      .then(data => {
        if (this.color) {
          this.svgCode = data.replace('<svg ', `<svg fill="${this.color}" `);
        } else {
          this.svgCode = data;
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleClick(event) {
    this.element.querySelectorAll('.nav-link').forEach(item => item.classList.remove('active'));
    this.element.querySelectorAll('.tab-pane').forEach(item => item.classList.remove('active'));
    event.target.classList.add('active');
    this.element.querySelector(event.target.getAttribute('href')).classList.add('active');
  }

  downloadIconAsPng() {
    try {
      // 1. Localisez le container de l'élément SVG.
      const iconContainer = document.querySelector('road-grid.toast-bar.isVisible road-icon') as HTMLElement;
      console.log(iconContainer);

      if (!iconContainer) {
        console.log('Icon container not found');
        return;
      }

      // Accédez au Shadow DOM du container.
      const shadowRoot = iconContainer.shadowRoot;
      if (!shadowRoot) {
        console.log('Shadow root not found');
        return;
      }

      // Accédez à l'élément SVG à l'intérieur du Shadow DOM.
      const svgElement = shadowRoot.querySelector('svg');
      if (!svgElement) {
        console.log('SVG element not found inside shadow root');
        return;
      }
      const elementsWithFill = svgElement.querySelectorAll('[fill]');
      elementsWithFill.forEach(element => {
        const computedFill = getComputedStyle(element).fill;
        if (computedFill) {
          element.setAttribute('fill', computedFill);
        }
      });

      // Convertir l'élément en SVG (si ce n'est pas déjà le cas).
      const svgData = new XMLSerializer().serializeToString(svgElement);
      const blob = new Blob([svgData], { type: 'image/svg+xml' });
      let url: string;
      if (window.URL && window.URL.createObjectURL) {
        url = window.URL.createObjectURL(blob);
      } else if (window.webkitURL && window.webkitURL.createObjectURL) {
        url = window.webkitURL.createObjectURL(blob);
      } else {
        throw new Error('Neither window.URL nor window.webkitURL is available.');
      }
      const img = new Image();

      img.onload = () => {
        const sizeSelector = document.getElementById('sizeSelector') as any;
        const selectedSize = parseInt(sizeSelector.value, 10);

        const canvas = document.createElement('canvas');
        canvas.width = selectedSize;
        canvas.height = selectedSize;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0, selectedSize, selectedSize);
        const pngData = canvas.toDataURL('image/png');

        // 3. Téléchargez les données PNG.
        const a = document.createElement('a');
        a.href = pngData;
        a.download = `${this.selectedIcon}.png`;
        a.click();

        // Nettoyez l'URL temporaire.
        if (window.URL && window.URL.createObjectURL) {
          window.URL.revokeObjectURL(url);
        } else if (window.webkitURL && window.webkitURL.createObjectURL) {
          window.webkitURL.revokeObjectURL(url);
        }
        console.log('Conversion and download completed');
      };

      // 2. Convertissez l'élément SVG en données PNG.
      img.src = url;
    } catch (error) {
      console.error('An error occurred:', error);
    }
  }

  render() {
    const sizesOptions = [
      { value: '64', label: '64x64' },
      { value: '128', label: '128x128' },
      { value: '256', label: '256x256' },
      { value: '512', label: '512x512' },
    ];
    return (
      <road-tabs>
        <road-tab-bar slot="top" selected-tab="tab-description">
          <road-tab-button tab="tab-svg">
            <road-label>SVG</road-label>
          </road-tab-button>

          <road-tab-button tab="tab-component">
            <road-label>Web Component</road-label>
          </road-tab-button>

          <road-tab-button tab="tab-png">
            <road-label>PNG</road-label>
          </road-tab-button>
        </road-tab-bar>

        <road-tab tab="tab-svg">
          <input-code input-value={this.svgCode}></input-code>
        </road-tab>

        <road-tab tab="tab-component">
          {this.color ? (
            <input-code input-value={`<road-icon name="${this.selectedIcon}" style="color: ${this.color}"></road-icon>`}></input-code>
          ) : (
            <input-code input-value={`<road-icon name="${this.selectedIcon}"></road-icon>`}></input-code>
          )}
        </road-tab>

        <road-tab tab="tab-png">
          <road-select id="sizeSelector" label="Size" options={sizesOptions}></road-select>
          <road-button color="primary" onClick={() => this.downloadIconAsPng()}>
            Download as PNG
          </road-button>
        </road-tab>
      </road-tabs>
    );
  }
}
