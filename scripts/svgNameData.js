'use strict';

const folder = './public/build/svg/';
const fs = require('fs');

let data = {};

fs.readdirSync(folder).forEach(file => {
  data[file.split('.').slice(0, -1).join('.').toLocaleLowerCase()] = "";
});

fs.writeFileSync('./src/data.json', JSON.stringify(data));