import { h, Component, Prop } from '@stencil/core';

@Component({
  tag: 'toast-bar',
  styleUrl: 'toast-bar.css',
})
export class ToastBar {
  @Prop() selectedIcon: string;

  @Prop() color: string;

  render() {
    return (
      <road-grid class={`toast-bar container pt-16 pb-16 ${this.selectedIcon ? 'isVisible' : ''}`} onClick={ev => ev.stopPropagation()}>
        <road-row>
          <road-col class="col-2 text-right d-none d-md-block">
            <road-icon name={this.selectedIcon} style={{ color: this.color }}></road-icon>
          </road-col>
          <road-col class="col-12 col-md-9">
            <h2 class="h5 mb-8">{this.selectedIcon}</h2>
            <tab-bar selectedIcon={this.selectedIcon} color={this.color}></tab-bar>
          </road-col>
        </road-row>
      </road-grid>
    );
  }
}
